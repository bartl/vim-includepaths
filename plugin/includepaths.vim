if exists('g:loaded_includepaths')
	finish
endif
let g:loaded_includepaths = 1

command! -n=0 IncludePathsReload :call s:LoadIncludePathsFile()
command! -n=0 AddCurrentPathToModifiables :call s:AddElementToAllModifiables(expand('%:p:h'))
command! -n=? AddCurrentPathToFile :call s:AddElementToFile(expand('%:p:h'), <args>)

function! s:GetIncludePathsSettingsFilePath()
	if !exists("g:includepaths_path_file")
		return $HOME.'/.vimincludes'
	else
		return g:includepaths_path_file
	endif
endfunction

function! s:ReplacePrefixes(path)
	if exists("g:includepaths_prefix_replacements")
		for [prefix, replacement] in items(g:includepaths_prefix_replacements)
			let s:replaced = substitute(a:path, prefix, replacement, "")
			if a:path != s:replaced
				return s:replaced
			endif
		endfor
		return a:path
	endif
endfunction

function! s:ClearSetVariable(variable)
	silent! execute "set ".a:variable."="
endfunction

function! s:ClearLetVariable(variable)
	if exists(a:variable)
		silent! execute "unlet ".a:variable
	endif
endfunction

function! s:ClearExisting()
	if exists("g:includepaths_clear_existing")
		if g:includepaths_clear_existing == 1
			return 1
		endif
	endif
	return 0
endfunction

function! s:GetModifiables()
	if exists("g:includepaths_settings_to_modify")
		return g:includepaths_settings_to_modify
	endif
	return {"path":"set"}
endfunction

function! s:ClearVariable(variable, type)
	if a:type == "set"
		call s:ClearSetVariable(a:variable)
	elseif a:type == "let"
		call s:ClearLetVariable(a:variable)
	else
		echoerr "Unknown type ".a:type." specified for variable ".a:variable
	endif
endfunction

function! s:ClearAllVariables(variables)
	for [variable, type] in items(a:variables)
		call s:ClearVariable(variable, type)
	endfor
endfunction

function! s:CreateOrAppendSetVariable(variable, value)
	silent! execute "set ".a:variable."+=".a:value
endfunction

function! s:CreateOrAppendLetVariable(variable, value)
	if exists(a:variable)
		silent! execute "call add(".a:variable.",'".a:value."')"
	else
		silent! execute "let ".a:variable."=['".a:value."']"
	endif
endfunction

function! s:CreateOrAppendVariable(variable, value, type)
	if a:type == "let"
		call s:CreateOrAppendLetVariable(a:variable, a:value)
	elseif a:type == "set"
		call s:CreateOrAppendSetVariable(a:variable, a:value)
	elseif a:type == "file"
		call s:AddElementToFile(a:value)
	else
		echoerr "Unknown type ".a:type." specified for variable ".a:variable
	endif
endfunction

function! s:LoadIncludePathsFile()
	let s:modifiables = s:GetModifiables()
	if s:ClearExisting() == 1
		call s:ClearAllVariables(s:modifiables)
	endif

	" Reads the file containing the include paths
	let s:f = s:GetIncludePathsSettingsFilePath()
	if filereadable(expand(s:f))
		for s:line in readfile(expand(s:f))
			call s:AddElementToAllModifiables(s:line)
		endfor
	else
		echoerr "File ".s:f. " is unreadable"
	endif
endfunction

function! s:AddElementToAllModifiables(element)
	let s:modelement=s:ReplacePrefixes(a:element)
	for [variable, type] in items(s:modifiables)
		call s:CreateOrAppendVariable(variable, s:modelement, type)
	endfor
endfunction

function! s:CreateOrAppendToFile(text, file)
	if filereadable(a:file)
		let l:toWrite = readfile(a:file, 'b') + [a:text]
	else
		let l:toWrite = [a:text]
	endif
	call s:SortUnique(l:toWrite)
	call writefile(l:toWrite, a:file, 'b')
endfunction

function! s:AddElementToFile(element, ...)
	let l:file = 'nofile'
	if a:0 == 1
		let l:file=expand(a:1)
	else
		if !exists("g:includepaths_add_file")
			let l:file=expand('~/.clang_complete')
		else
			let l:file=expand(g:includepaths_add_file)
		endif
	endif
	let l:element = a:element
	if exists("g:includepaths_file_prefix")
		let l:element = g:includepaths_file_prefix . a:element
	endif
	call s:CreateOrAppendToFile(l:element, l:file)
endfunction

function! s:SortUnique(list, ...)
	let dictionary = {}
	for i in a:list
		let dictionary[string(i)] = i
	endfor
	let result = []
	if exists('a:1')
		let result = sort(values(dictionary), a:1 )
	else
		let result = sort(values(dictionary))
	endif
	return result
endfunction


autocmd VimEnter * call s:LoadIncludePathsFile()
